%Int. Lineal
%esto convierte a x en una variable simbolica
syms x

f=log(x);

%aqui se pide el intervalo [a,b]
a=input('ingrese el intervalo inferior: ');

while a<=0
    fprintf('No debe ser menor o igual a cero');
    a=input('ingrese el intervalo inferior: ');
end

b=input('ingrese el intervalo superior: ');

while b<=0 || b<a
    fprintf('No debe ser menor o igual a cero o menor al anterior ');
    b=input('ingrese el intervalo superior: ');
end

grado=input('ingrese el grado del polinomio: ');

while grado < 1
    fprintf('Debe ser al menos cero');
    grado=input('ingrese el grado del polinomio: '); 
end

puntos(1)=a;
puntos(2)=b;

if(grado==1)
valoresX=puntos;

else
    while (grado+1)>numel(puntos)
        tamanio=numel(puntos);
        for i=1:1:tamanio-1
            puntos(tamanio+i)=(puntos(i)+puntos(i+1))/2;
        end

        if (grado+1)>numel(puntos)
           puntos=sort(puntos);
        else
            for i=1:1:grado+1
                valoresX(i)=puntos(i);
            end
            valoresX=sort(valoresX);
        end

    end   
end


for i=1:1:numel(valoresX)
   valoresY(i)=vpa(subs(f,valoresX(i))); 
end



