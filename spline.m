
syms x
syms a b c d e f g h i j k l m n o p q r s t u v w x y z
syms cz

f=cos(x);

%aqui se pide el intervalo [a,b]
aa=input('ingrese el intervalo inferior: ');
bb=input('ingrese el intervalo superior: ');


h=(bb-aa)/3;
p1=aa;
p2=p1+h;
p3=p2+h;
p4=p3+h;
y1=subs(f,p1);
y2=subs(f,p2);
y3=subs(f,p3);
y4=subs(f,p4);

itv1=[p1 p2];
itv2=[p2 p3];
itv3=[p3 p4];

grado=input('ingrese el grado de polinomio que desea: (de 1 a 4) : ');
letras=[a b c d e f g h i j k l m n o p q r s t u v w x y z];
w=0;
Ep=0;
for i=grado:-1:0
  w=w+1;
  Ep=Ep + letras(1,w)*x^(i);
end  

numeroecuaciones=(grado+1)*3;
intervalop=[itv1 itv2 itv3];
for i=1:1:6
  if(i<=6) 
  switch(i)
    case 1
      e1=coeffs(subs(Ep,x,intervalop(1,i)));
    case 2
      e2=coeffs(subs(Ep,x,intervalop(1,i)));
    case 3 
      e3=coeffs(subs(Ep,x,intervalop(1,i)));
    case 4 
      e4=coeffs(subs(Ep,x,intervalop(1,i)));
    case 5 
      e5=coeffs(subs(Ep,x,intervalop(1,i)));
    case 6 
      e6=coeffs(subs(Ep,x,intervalop(1,i)));
  end
  end
end   


switch(grado)
  case 1

    ee1=[e1 0 0 0 0];
    ee2=[e2 0 0 0 0];
    ee3=[0 0 e3 0 0];
    ee4=[0 0 e4 0 0];
    ee5=[0 0 0 0 e5];
    ee6=[0 0 0 0 e6];
   
    
    
    tabla=[ee1; ee2; ee3; ee4; ee5; ee6];
    cc=[y1; y2; y2; y3; y3; y4];
    valores=(tabla\cc);
    
   
    z1=subs(Ep,a,valores(1,1));
    z1=subs(z1,b,valores(2,1));
    z2=subs(Ep,a,valores(3,1));
    z2=subs(z2,b,valores(4,1));
    z3=subs(Ep,a,valores(5,1));
    z3=subs(z3,b,valores(6,1));
    
    fprintf('las ecuaciones quedan: \n');
    
    fprintf('  en el intervalo [%f,%f] \n',itv1);
    disp(vpa(z1));
    
    fprintf('  en el intervalo [%f,%f] \n',itv2);
    disp(vpa(z2));
    
    fprintf('  en el intervalo [%f,%f] \n',itv3);
    disp(vpa(z3));
    
  case 2
   d1=diff(Ep,x);
   e7=[coeffs(subs(d1,x,intervalop(1,2))) 0 coeffs(-1*(subs(d1,x,intervalop(1,2))))];
   e8=[coeffs(subs(d1,x,intervalop(1,3))) 0 coeffs(-1*(subs(d1,x,intervalop(1,3))))];
   
   e1(1,1)=0;
   e2(1,1)=0;
   e7(1,1)=0;
   e1=e1(2:3);
   e2=e2(2:3);
   e7=e7(2:5);
   
   
    ee1=[e1 0 0 0 0 0 0];
    ee2=[e2 0 0 0 0 0 0];
    ee3=[0 0 e3 0 0 0];
    ee4=[0 0 e4 0 0 0];
    ee5=[0 0 0 0 0 e5];
    ee6=[0 0 0 0 0 e6];
    ee7=[e7 0 0 0 0];
    ee8=[0 0 e8 0];
    
    
    tabla=[ee1; ee2; ee3; ee4; ee5; ee6; ee7; ee8];
    cc=[y1; y2; y2; y3; y3; y4; 0; 0];
    valores=(tabla\cc);
    
    
    z1=subs(Ep,a,0);
    z1=subs(z1,b,valores(1,1));
    z1=subs(z1,c,valores(2,1));
    z2=subs(Ep,a,valores(3,1));
    z2=subs(z2,b,valores(4,1));
    z2=subs(z2,c,valores(5,1));
    z3=subs(Ep,a,valores(6,1));
    z3=subs(z3,b,valores(7,1));
    z3=subs(z3,c,valores(8,1));
    
    
    fprintf('las ecuaciones quedan: \n');
    
    fprintf('  en el intervalo [%f,%f] \n',itv1);
    disp(vpa(z1));
    
    fprintf('  en el intervalo [%f,%f] \n',itv2);
    disp(vpa(z2));
    
    fprintf('  en el intervalo [%f,%f] \n',itv3);
    disp(vpa(z3));
    
  case 3
    
    d1=diff(Ep,x);
   e7=[coeffs(subs(d1,x,intervalop(1,2))) 0 coeffs(-1*(subs(d1,x,intervalop(1,2))))];
   e8=[coeffs(subs(d1,x,intervalop(1,3))) 0 coeffs(-1*(subs(d1,x,intervalop(1,3))))];
   
   d2=diff(d1,x);
   e9=[coeffs(subs(d2,x,intervalop(1,2))) 0 0 coeffs(-1*(subs(d2,x,intervalop(1,2))))];
   e10=[coeffs(subs(d2,x,intervalop(1,3))) 0 0 coeffs(-1*(subs(d2,x,intervalop(1,3))))];
   
   e11=coeffs(subs(d2,x,intervalop(1,1)));
   e12=coeffs(subs(d2,x,intervalop(1,4)));
   
   
    ee1=[e1 0 0 0 0 0 0 0 0];
    ee2=[e2 0 0 0 0 0 0 0 0];
    ee3=[0 0 0 0 e3 0 0 0 0];
    ee4=[0 0 0 0 e4 0 0 0 0];
    ee5=[0 0 0 0 0 0 0 0 e5];
    ee6=[0 0 0 0 0 0 0 0 e6];
    ee7=[e7 0 0 0 0 0];
    ee8=[0 0 0 0 e8 0];
    ee9=[e9 0 0 0 0 0 0];
    ee10=[0 0 0 0 e10 0 0];
    ee11=[e11 0 0 0 0 0 0 0 0 0 0];
    ee12=[0 0 0 0 0 0 0 0 e12 0 0];
  
    tabla=[ee1; ee2; ee3; ee4; ee5; ee6; ee7; ee8; ee9; ee10; ee11; ee12];
    cc=[y1; y2; y2; y3; y3; y4; 0; 0; 0; 0; 0; 0];
    valores=(tabla\cc);
    
    
    z1=subs(Ep,a,valores(1,1));
    z1=subs(z1,b,valores(2,1));
    z1=subs(z1,c,valores(3,1));
    z1=subs(z1,d,valores(4,1));
    z2=subs(Ep,a,valores(5,1));
    z2=subs(z2,b,valores(6,1));
    z2=subs(z2,c,valores(7,1));
    z2=subs(z2,d,valores(8,1));
    z3=subs(Ep,a,valores(9,1));
    z3=subs(z3,b,valores(10,1));
    z3=subs(z3,c,valores(11,1));
    z3=subs(z3,d,valores(12,1));
    
    
    fprintf('las ecuaciones quedan: \n');
    
    fprintf('  en el intervalo [%f,%f] \n',itv1);
    disp(vpa(z1));
    
    fprintf('  en el intervalo [%f,%f] \n',itv2);
    disp(vpa(z2));
    
    fprintf('  en el intervalo [%f,%f] \n',itv3);
    disp(vpa(z3));
    
    
  case 4
    d1=diff(Ep,x);
   e7=[coeffs(subs(d1,x,intervalop(1,2))) 0 coeffs(-1*(subs(d1,x,intervalop(1,2))))];
   e8=[coeffs(subs(d1,x,intervalop(1,3))) 0 coeffs(-1*(subs(d1,x,intervalop(1,3))))];
   
   d2=diff(d1,x);
   e9=[coeffs(subs(d2,x,intervalop(1,2))) 0 0 coeffs(-1*(subs(d2,x,intervalop(1,2))))];
   e10=[coeffs(subs(d2,x,intervalop(1,3))) 0 0 coeffs(-1*(subs(d2,x,intervalop(1,3))))];
   
   d3=diff(d2,x);
   e11=[coeffs(subs(d3,x,intervalop(1,2))) 0 0 0 coeffs(subs(d3,x,intervalop(1,2)))];
   e12=[coeffs(subs(d3,x,intervalop(1,3))) 0 0 0 coeffs(subs(d3,x,intervalop(1,3)))];
   
   
   e13=coeffs(subs(d3,x,intervalop(1,1)));
   e14=coeffs(subs(d3,x,intervalop(1,4)));
   
   e1(1,1)=0;
   e2(1,1)=0;
   e7(1,1)=0;
   e9(1,1)=0;
   e11(1,1)=0;
   e13(1,1)=0;
   e1=e1(2:5);
   e2=e2(2:5);
   e7=e7(2:9);
   e9=e9(2:8);
   e11=e11(2:7);
   e13=e13(2:2);
   
    ee1=[e1 0 0 0 0 0 0 0 0 0 0];
    ee2=[e2 0 0 0 0 0 0 0 0 0 0];
    ee3=[0 0 0 0 e3 0 0 0 0 0];
    ee4=[0 0 0 0 e4 0 0 0 0 0];
    ee5=[0 0 0 0 0 0 0 0 0 e5];
    ee6=[0 0 0 0 0 0 0 0 0 e6];
    ee7=[e7 0 0 0 0 0 0];
    ee8=[0 0 0 0 e8 0];
    ee9=[e9 0 0 0 0 0 0 0];
    ee10=[0 0 0 0 e10 0 0];
    ee11=[e11 0 0 0 0 0 0 0 0];
    ee12=[0 0 0 0 e12 0 0 0];
    ee13=[e13 0 0 0 0 0 0 0 0 0 0 0 0 0];
    ee14=[0 0 0 0 0 0 0 0 0 e14 0 0 0];
  
    tabla=[ee1; ee2; ee3; ee4; ee5; ee6; ee7; ee8; ee9; ee10; ee11; ee12; ee13; ee14];
    cc=[y1; y2; y2; y3; y3; y4; 0; 0; 0; 0; 0; 0; 0; 0];
    valores=(tabla\cc);
    
    
    z1=subs(Ep,a,0);
    z1=subs(z1,b,valores(1,1));
    z1=subs(z1,c,valores(2,1));
    z1=subs(z1,d,valores(3,1));
    z1=subs(z1,e,valores(4,1));
    z2=subs(Ep,a,valores(5,1));
    z2=subs(z2,b,valores(6,1));
    z2=subs(z2,c,valores(7,1));
    z2=subs(z2,d,valores(8,1));
    z2=subs(z2,e,valores(9,1));
    z3=subs(Ep,a,valores(10,1));
    z3=subs(z3,b,valores(11,1));
    z3=subs(z3,c,valores(12,1));
    z3=subs(z3,d,valores(13,1));
    z3=subs(z3,e,valores(14,1));
    
    fprintf('las ecuaciones quedan: \n');
    
    fprintf('  en el intervalo [%f,%f] \n',itv1);
    disp(vpa(z1));
    
    fprintf('  en el intervalo [%f,%f] \n',itv2);
    disp(vpa(z2));
    
    fprintf('  en el intervalo [%f,%f] \n',itv3);
    disp(vpa(z3));
    

end

%esto de aqui grafica la funcion de manera simple
    
ezplot(z1,[aa,bb]);
ylabel('Y');
xlabel('X');
grid on 
hold on 
ezplot(z2,[aa,bb]);
ezplot(z3,[aa,bb]);
ezplot(f,[aa,bb]);


