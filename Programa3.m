%esto convierte las variable en variables simbolicas
syms x
syms a b c d e f g h i j k l m n o p q r s t u v w x y z
syms cz

fun=input('Ingrese la ecuacion que desea utilizar\n1. Ln(x)\n2. cos(x)\n3. arctan(x)\n4. ((1+x)/(x^3+2x))^1/2\n5. e^(x^1/2)\n');

while fun <1 || fun >5
    clc
    fun=input('Ingrese la ecuacion que desea utilizar\n1. Ln(x)\n2. cos(x)\n3. arctan(x)\n4. ((1+x)/(x^3+2x))^1/2\n5. e^(x^1/2)\n');
end

switch fun
    case 1  %Ln(x)
        f=log(x);
        %aqui se pide el intervalo [aa,bb]
        aa=input('ingrese el intervalo inferior: ');

        while aa<=0
            fprintf('No debe ser menor o igual a cero\n');
            aa=input('ingrese el intervalo inferior: ');
        end

        bb=input('ingrese el intervalo superior: ');

        while bb<=0 || bb<=aa
            fprintf('No debe ser menor o igual a cero o menor al anterior\n');
            bb=input('ingrese el intervalo superior: ');
        end
        
    case 2  %Cos(x)
        f=cos(x);
        %aqui se pide el intervalo [aa,bb]
        aa=input('ingrese el intervalo inferior: ');

        bb=input('ingrese el intervalo superior: ');

        while bb<=aa
            fprintf('No debe ser menor al anterior\n');
            bb=input('ingrese el intervalo superior: ');
        end
        
    case 3  %arctan(x)
        f=atan(x);
        %aqui se pide el intervalo [aa,bb]
        aa=input('ingrese el intervalo inferior: ');

        bb=input('ingrese el intervalo superior: ');

        while bb<=aa
            fprintf('No debe ser menor al anterior\n');
            bb=input('ingrese el intervalo superior: ');
        end
        
    case 4  %((1+x)/(x^3+2x))^1/2
        f=((1+x)/(x^3+2*x))^1/2;
         %aqui se pide el intervalo [aa,bb]
        aa=input('ingrese el intervalo inferior: ');

        while aa<=0
            fprintf('No debe ser menor o igual a cero\n');
            aa=input('ingrese el intervalo inferior: ');
        end

        bb=input('ingrese el intervalo superior: ');

        while bb<=0 || bb<=aa
            fprintf('No debe ser menor o igual a cero o menor al anterior\n');
            bb=input('ingrese el intervalo superior: ');
        end
        
        
    case 5  %e^(x^1/2)
        f=exp(x^(1/2));
        %aqui se pide el intervalo [aa,bb]
        aa=input('ingrese el intervalo inferior: ');

        while aa<0
            fprintf('No debe ser menor a cero\n');
            aa=input('ingrese el intervalo inferior: ');
        end

        bb=input('ingrese el intervalo superior: ');

        while bb<0 || bb<=aa
            fprintf('No debe ser menor o igual a cero o menor al anterior\n');
            bb=input('ingrese el intervalo superior: ');
        end
        
        
end

% Selecciona el metodo
opcion=input('seleccione el metodo \n1. Lineal\n2. Cuadratica\n3. Lagrange\n4. Newton\n5. Hermite\n6. Spline\n');

while opcion<1 || opcion>6
    clc
    opcion=input('seleccione el metodo \n1. Lineal\n2. Cuadratica\n3. Lagrange\n4. Newton\n5. Hermite\n6. Spline\n');
end
switch opcion
    case 1  %Lineal
        
        valoresX(1)=aa;
        valoresX(2)=bb;

        valoresY(1)=vpa(subs(f,valoresX(1)));
        valoresY(2)=vpa(subs(f,valoresX(2)));


        Px=valoresY(1)+((valoresY(2)-valoresY(1))/(valoresX(2)-valoresX(1)))*(x-valoresX(1));

        fprintf('P(x)= ');
        disp(vpa(expand(Px),15));
        
        
        ezplot(Px,[aa,bb]);
        ylabel('Y');
        xlabel('X');
        grid on 
        hold on 
        ezplot(f,[aa,bb]);
        
    case 2  %Cuadratica
        
        valoresX(1)=aa;
        valoresX(2)=(aa+bb)/2;
        valoresX(3)=bb;

        valoresY(1)=vpa(subs(f,valoresX(1)));
        valoresY(2)=vpa(subs(f,valoresX(2)));
        valoresY(3)=vpa(subs(f,valoresX(3)));

        b0=valoresY(1);
        b1=(valoresY(2)-valoresY(1))/(valoresX(2)-valoresX(1));
        b2=((valoresY(3)-valoresY(2))/(valoresX(3)-valoresX(2))-b1)/(valoresX(3)-valoresX(1));

        Px=b0+b1*(x-valoresX(1))+b2*(x-valoresX(1))*(x-valoresX(2));

        fprintf('P(x)= ');
        disp(vpa(expand(Px),15));
        
        
        ezplot(Px,[aa,bb]);
        ylabel('Y');
        xlabel('X');
        grid on 
        hold on 
        ezplot(f,[aa,bb]);
        
    case 3  %Lagrange
        
        grado=input('ingrese el grado del polinomio: ');

        while grado < 1
            fprintf('Debe ser al menos cero\n');
            grado=input('ingrese el grado del polinomio: '); 
        end
        
        puntos(1)=aa;
        puntos(2)=bb;

        if(grado==1)
        valoresX=puntos;

        else
            while (grado+1)>numel(puntos)
                tamanio=numel(puntos);
                for i=1:1:tamanio-1
                    puntos(tamanio+i)=(puntos(i)+puntos(i+1))/2;
                end

                if (grado+1)>numel(puntos)
                   puntos=sort(puntos);
                else
                    for i=1:1:grado+1
                        valoresX(i)=puntos(i);
                    end
                    valoresX=sort(valoresX);
                end

            end   
        end

        Px=x-x;

        for i=1:1:numel(valoresX)
           valoresY(i)=vpa(subs(f,valoresX(i))); 
        end

        for i=1:1:grado+1
            dato=valoresY(i);
            for j=1:1:grado+1
               if  i==j
               else
                   dato=dato*(x-valoresX(j))/(valoresX(i)-valoresX(j));
               end
            end
            Px=Px+dato;
        end


        fprintf('P(x)= ');
        disp(vpa(expand(Px),15));

        % Calculo de errores
        opcion=input('Desea calcular los errores ?\n1.Si\nOtro No\n');
        if opcion==1
            clc
            valor=input('ingrese el valor de x\n');
            while(valor<aa || valor>bb)
               valor=input('ingrese el valor de x\n');     
            end
            P=subs(Px,valor);

            %Errores
            fprintf('P(%f)= ',valor);
            disp(vpa(P,15));
            
            switch fun
                
                case 1
                    vr=log(valor);    
                case 2
                    vr=cos(valor);
                case 3
                    vr=atan(valor);
                case 4
                    vr=((1+valor)/(valor^3+2*valor))^1/2;
                case 5
                    vr=exp(valor^(1/2));
            end
            
            Ev=abs(vr-P);
            Er=abs(Ev/vr);
            Ep=Er*100;

            derivada=f;


            for i=1:1:grado+1
                derivada=diff(derivada);
            end

            Et=vpa(subs(derivada,valoresX(grado+1))/(factorial(grado+2)));

            for i=1:1:grado+1
                Et=abs(Et*(valor-valoresX(i)));
            end

            fprintf('Ev= ');
            disp(vpa(Ev,15));

            fprintf('Er= ');
            disp(vpa(Er,15));

            fprintf('Ep= ');
            disp(vpa(Ep,15));

            fprintf('E.Teorico= ');
            disp(vpa(Et,15));

        end

        ezplot(Px,[aa,bb]);
        ylabel('Y');
        xlabel('X');
        grid on 
        hold on 
        ezplot(f,[aa,bb]);

        
    case 4  %Newton
        
        grado=input('ingrese el grado del polinomio: ');

        while grado < 1
            fprintf('Debe ser al menos 1\n');
            grado=input('ingrese el grado del polinomio: '); 
        end

        puntos(1)=aa;
        puntos(2)=bb;

        if(grado==1)
        valoresX=puntos;

        else
            %Valores del intervalo
            while (grado+1)>numel(puntos)
                tamanio=numel(puntos);
                for i=1:1:tamanio-1
                    puntos(tamanio+i)=(puntos(i)+puntos(i+1))/2;
                end

                if (grado+1)>numel(puntos)
                   puntos=sort(puntos);
                else
                    for i=1:1:grado+1
                        valoresX(i)=puntos(i);
                    end
                    valoresX=sort(valoresX);
                end

            end   
        end

        %valores de y
        for i=1:1:numel(valoresX)
           tabla(i,1)=vpa(subs(f,valoresX(i))); 
        end

        %Escalera
        for i=2:1:grado+1
            for j=i:1:grado+1
                tabla(j,i)=(tabla(j,i-1)-tabla(j-1,i-1))/(valoresX(j)-valoresX(j-(i-1)));
            end

        end


        Px=tabla(1,1);
        for i=2:1:grado+1
            dato=tabla(i,i);
            for j=1:1:grado
                dato=dato*(x-valoresX(j));    
            end
            Px=Px+dato;
        end

        fprintf('P(x)= ');
        disp(vpa(expand(Px),15));

        % calculo de errores
        opcion=input('Desea calcular los errores ?\n1.Si\nOtro No\n');
        if opcion==1
            clc
            valor=input('ingrese el valor de x\n');
            while(valor<aa || valor>bb)
               valor=input('ingrese el valor de x\n');     
            end
            P=subs(Px,valor);

            %Errores
            fprintf('P(%f)= ',valor);
            disp(vpa(P,15));
            
            switch fun
                
                case 1
                    vr=log(valor);    
                case 2
                    vr=cos(valor);
                case 3
                    vr=atan(valor);
                case 4
                    vr=((1+valor)/(valor^3+2*valor))^1/2;
                case 5
                    vr=exp(valor^(1/2));
            end
            
            Ev=abs(vr-P);
            Er=abs(Ev/vr);
            Ep=Er*100;

            derivada=f;


            for i=1:1:grado+1
                derivada=diff(derivada);
            end

            Et=vpa(subs(derivada,valoresX(grado+1))/(factorial(grado+2)));

            for i=1:1:grado+1
                Et=abs(Et*(valor-valoresX(i)));
            end

            fprintf('Ev= ');
            disp(vpa(Ev,15));

            fprintf('Er= ');
            disp(vpa(Er,15));

            fprintf('Ep= ');
            disp(vpa(Ep,15));

            fprintf('E.Teorico= ');
            disp(vpa(Et,15));

        end

        ezplot(Px,[aa,bb]);
        ylabel('Y');
        xlabel('X');
        grid on 
        hold on 
        ezplot(f,[aa,bb]);
        
        
    case 5  %Hermite
        
        grado=input('ingrese el grado del polinomio: ');

        while grado < 1
            fprintf('Debe ser al menos 1\n');
            grado=input('ingrese el grado del polinomio: '); 
        end

        fprintf('Donde desea aplicar las derivadas\n%f\n%f\n',aa,bb);
        valor=input('');

        while valor~=aa && valor~=bb
            fprintf('Debe ser %f o %f',aa,bb);
            valor=input('');
        end

        valoresX(1)=aa;
        valoresX(2)=bb;
        for i=3:1:grado+1
            valoresX(i)=valor;
        end

        valoresX=sort(valoresX);

        %valores de y
        for i=1:1:numel(valoresX)
           tabla(i,1)=vpa(subs(f,valoresX(i))); 
        end

        derivada=f;

        %Escalera
        for i=2:1:grado+1
            for j=i:1:grado+1
                if (valoresX(j)-valoresX(j-(i-1)))==0
                    derivada=diff(derivada);
                    tabla(j,i)=vpa(subs(derivada,valor))/factorial(i-1);
                else
                    tabla(j,i)=(tabla(j,i-1)-tabla(j-1,i-1))/(valoresX(j)-valoresX(j-(i-1))); 
                end

            end

        end

        Px=tabla(1,1);
        for i=2:1:grado+1
            dato=tabla(i,i);
            for j=1:1:grado
                dato=dato*(x-valoresX(j));    
            end
            Px=Px+dato;
        end

        fprintf('P(x)= ');
        disp(vpa(expand(Px),15));

        opcion=input('Desea calcular los errores ?\n1.Si\nOtro No\n');
        if opcion==1
            clc
            valor=input('ingrese el valor de x\n');
            while(valor<aa || valor>bb)
               valor=input('ingrese el valor de x\n');     
            end
            P=subs(Px,valor);

            %Errores
            fprintf('P(%f)= ',valor);
            disp(vpa(P,15));
            
            switch fun
                
                case 1
                    vr=log(valor);    
                case 2
                    vr=cos(valor);
                case 3
                    vr=atan(valor);
                case 4
                    vr=((1+valor)/(valor^3+2*valor))^1/2;
                case 5
                    vr=exp(valor^(1/2));
            end
            
            Ev=abs(vr-P);
            Er=abs(Ev/vr);
            Ep=Er*100;

            derivada=f;


            for i=1:1:grado+1
                derivada=diff(derivada);
            end

            Et=vpa(subs(derivada,valoresX(grado+1))/(factorial(grado+2)));

            for i=1:1:grado+1
                Et=abs(Et*(valor-valoresX(i)));
            end

            fprintf('Ev= ');
            disp(vpa(Ev,15));

            fprintf('Er= ');
            disp(vpa(Er,15));

            fprintf('Ep= ');
            disp(vpa(Ep,15));

            fprintf('E.Teorico= ');
            disp(vpa(Et,15));

        end

        ezplot(Px,[aa,bb]);
        ylabel('Y');
        xlabel('X');
        grid on 
        hold on 
        ezplot(f,[aa,bb]);


        
    case 6  %Spline
        
        h=(bb-aa)/3;
        p1=aa;
        p2=p1+h;
        p3=p2+h;
        p4=p3+h;
        y1=subs(f,p1);
        y2=subs(f,p2);
        y3=subs(f,p3);
        y4=subs(f,p4);

        itv1=[p1 p2];
        itv2=[p2 p3];
        itv3=[p3 p4];

        grado=input('ingrese el grado de polinomio que desea: (de 1 a 4) : ');
        letras=[a b c d e f g h i j k l m n o p q r s t u v w x y z];
        w=0;
        Ep=0;
        for i=grado:-1:0
          w=w+1;
          Ep=Ep + letras(1,w)*x^(i);
        end  

        numeroecuaciones=(grado+1)*3;
        intervalop=[itv1 itv2 itv3];
        for i=1:1:6
          if(i<=6) 
          switch(i)
            case 1
              e1=coeffs(subs(Ep,x,intervalop(1,i)));
            case 2
              e2=coeffs(subs(Ep,x,intervalop(1,i)));
            case 3 
              e3=coeffs(subs(Ep,x,intervalop(1,i)));
            case 4 
              e4=coeffs(subs(Ep,x,intervalop(1,i)));
            case 5 
              e5=coeffs(subs(Ep,x,intervalop(1,i)));
            case 6 
              e6=coeffs(subs(Ep,x,intervalop(1,i)));
          end
          end
        end   


        switch(grado)
          case 1

            ee1=[e1 0 0 0 0];
            ee2=[e2 0 0 0 0];
            ee3=[0 0 e3 0 0];
            ee4=[0 0 e4 0 0];
            ee5=[0 0 0 0 e5];
            ee6=[0 0 0 0 e6];



            tabla=[ee1; ee2; ee3; ee4; ee5; ee6];
            cc=[y1; y2; y2; y3; y3; y4];
            valores=(tabla\cc);


            z1=subs(Ep,a,valores(1,1));
            z1=subs(z1,b,valores(2,1));
            z2=subs(Ep,a,valores(3,1));
            z2=subs(z2,b,valores(4,1));
            z3=subs(Ep,a,valores(5,1));
            z3=subs(z3,b,valores(6,1));

            fprintf('las ecuaciones quedan: \n');

            fprintf('  en el intervalo [%f,%f] \n',itv1);
            disp(vpa(z1));

            fprintf('  en el intervalo [%f,%f] \n',itv2);
            disp(vpa(z2));

            fprintf('  en el intervalo [%f,%f] \n',itv3);
            disp(vpa(z3));

          case 2
           d1=diff(Ep,x);
           e7=[coeffs(subs(d1,x,intervalop(1,2))) 0 coeffs(-1*(subs(d1,x,intervalop(1,2))))];
           e8=[coeffs(subs(d1,x,intervalop(1,3))) 0 coeffs(-1*(subs(d1,x,intervalop(1,3))))];

           e1(1,1)=0;
           e2(1,1)=0;
           e7(1,1)=0;
           e1=e1(2:3);
           e2=e2(2:3);
           e7=e7(2:5);


            ee1=[e1 0 0 0 0 0 0];
            ee2=[e2 0 0 0 0 0 0];
            ee3=[0 0 e3 0 0 0];
            ee4=[0 0 e4 0 0 0];
            ee5=[0 0 0 0 0 e5];
            ee6=[0 0 0 0 0 e6];
            ee7=[e7 0 0 0 0];
            ee8=[0 0 e8 0];


            tabla=[ee1; ee2; ee3; ee4; ee5; ee6; ee7; ee8];
            cc=[y1; y2; y2; y3; y3; y4; 0; 0];
            valores=(tabla\cc);


            z1=subs(Ep,a,0);
            z1=subs(z1,b,valores(1,1));
            z1=subs(z1,c,valores(2,1));
            z2=subs(Ep,a,valores(3,1));
            z2=subs(z2,b,valores(4,1));
            z2=subs(z2,c,valores(5,1));
            z3=subs(Ep,a,valores(6,1));
            z3=subs(z3,b,valores(7,1));
            z3=subs(z3,c,valores(8,1));


            fprintf('las ecuaciones quedan: \n');

            fprintf('  en el intervalo [%f,%f] \n',itv1);
            disp(vpa(z1));

            fprintf('  en el intervalo [%f,%f] \n',itv2);
            disp(vpa(z2));

            fprintf('  en el intervalo [%f,%f] \n',itv3);
            disp(vpa(z3));

          case 3

            d1=diff(Ep,x);
           e7=[coeffs(subs(d1,x,intervalop(1,2))) 0 coeffs(-1*(subs(d1,x,intervalop(1,2))))];
           e8=[coeffs(subs(d1,x,intervalop(1,3))) 0 coeffs(-1*(subs(d1,x,intervalop(1,3))))];

           d2=diff(d1,x);
           e9=[coeffs(subs(d2,x,intervalop(1,2))) 0 0 coeffs(-1*(subs(d2,x,intervalop(1,2))))];
           e10=[coeffs(subs(d2,x,intervalop(1,3))) 0 0 coeffs(-1*(subs(d2,x,intervalop(1,3))))];

           e11=coeffs(subs(d2,x,intervalop(1,1)));
           e12=coeffs(subs(d2,x,intervalop(1,4)));


            ee1=[e1 0 0 0 0 0 0 0 0];
            ee2=[e2 0 0 0 0 0 0 0 0];
            ee3=[0 0 0 0 e3 0 0 0 0];
            ee4=[0 0 0 0 e4 0 0 0 0];
            ee5=[0 0 0 0 0 0 0 0 e5];
            ee6=[0 0 0 0 0 0 0 0 e6];
            ee7=[e7 0 0 0 0 0];
            ee8=[0 0 0 0 e8 0];
            ee9=[e9 0 0 0 0 0 0];
            ee10=[0 0 0 0 e10 0 0];
            ee11=[e11 0 0 0 0 0 0 0 0 0 0];
            ee12=[0 0 0 0 0 0 0 0 e12 0 0];

            tabla=[ee1; ee2; ee3; ee4; ee5; ee6; ee7; ee8; ee9; ee10; ee11; ee12];
            cc=[y1; y2; y2; y3; y3; y4; 0; 0; 0; 0; 0; 0];
            valores=(tabla\cc);


            z1=subs(Ep,a,valores(1,1));
            z1=subs(z1,b,valores(2,1));
            z1=subs(z1,c,valores(3,1));
            z1=subs(z1,d,valores(4,1));
            z2=subs(Ep,a,valores(5,1));
            z2=subs(z2,b,valores(6,1));
            z2=subs(z2,c,valores(7,1));
            z2=subs(z2,d,valores(8,1));
            z3=subs(Ep,a,valores(9,1));
            z3=subs(z3,b,valores(10,1));
            z3=subs(z3,c,valores(11,1));
            z3=subs(z3,d,valores(12,1));


            fprintf('las ecuaciones quedan: \n');

            fprintf('  en el intervalo [%f,%f] \n',itv1);
            disp(vpa(z1));

            fprintf('  en el intervalo [%f,%f] \n',itv2);
            disp(vpa(z2));

            fprintf('  en el intervalo [%f,%f] \n',itv3);
            disp(vpa(z3));


          case 4
            d1=diff(Ep,x);
           e7=[coeffs(subs(d1,x,intervalop(1,2))) 0 coeffs(-1*(subs(d1,x,intervalop(1,2))))];
           e8=[coeffs(subs(d1,x,intervalop(1,3))) 0 coeffs(-1*(subs(d1,x,intervalop(1,3))))];

           d2=diff(d1,x);
           e9=[coeffs(subs(d2,x,intervalop(1,2))) 0 0 coeffs(-1*(subs(d2,x,intervalop(1,2))))];
           e10=[coeffs(subs(d2,x,intervalop(1,3))) 0 0 coeffs(-1*(subs(d2,x,intervalop(1,3))))];

           d3=diff(d2,x);
           e11=[coeffs(subs(d3,x,intervalop(1,2))) 0 0 0 coeffs(subs(d3,x,intervalop(1,2)))];
           e12=[coeffs(subs(d3,x,intervalop(1,3))) 0 0 0 coeffs(subs(d3,x,intervalop(1,3)))];


           e13=coeffs(subs(d3,x,intervalop(1,1)));
           e14=coeffs(subs(d3,x,intervalop(1,4)));

           e1(1,1)=0;
           e2(1,1)=0;
           e7(1,1)=0;
           e9(1,1)=0;
           e11(1,1)=0;
           e13(1,1)=0;
           e1=e1(2:5);
           e2=e2(2:5);
           e7=e7(2:9);
           e9=e9(2:8);
           e11=e11(2:7);
           e13=e13(2:2);

            ee1=[e1 0 0 0 0 0 0 0 0 0 0];
            ee2=[e2 0 0 0 0 0 0 0 0 0 0];
            ee3=[0 0 0 0 e3 0 0 0 0 0];
            ee4=[0 0 0 0 e4 0 0 0 0 0];
            ee5=[0 0 0 0 0 0 0 0 0 e5];
            ee6=[0 0 0 0 0 0 0 0 0 e6];
            ee7=[e7 0 0 0 0 0 0];
            ee8=[0 0 0 0 e8 0];
            ee9=[e9 0 0 0 0 0 0 0];
            ee10=[0 0 0 0 e10 0 0];
            ee11=[e11 0 0 0 0 0 0 0 0];
            ee12=[0 0 0 0 e12 0 0 0];
            ee13=[e13 0 0 0 0 0 0 0 0 0 0 0 0 0];
            ee14=[0 0 0 0 0 0 0 0 0 e14 0 0 0];

            tabla=[ee1; ee2; ee3; ee4; ee5; ee6; ee7; ee8; ee9; ee10; ee11; ee12; ee13; ee14];
            cc=[y1; y2; y2; y3; y3; y4; 0; 0; 0; 0; 0; 0; 0; 0];
            valores=(tabla\cc);


            z1=subs(Ep,a,0);
            z1=subs(z1,b,valores(1,1));
            z1=subs(z1,c,valores(2,1));
            z1=subs(z1,d,valores(3,1));
            z1=subs(z1,e,valores(4,1));
            z2=subs(Ep,a,valores(5,1));
            z2=subs(z2,b,valores(6,1));
            z2=subs(z2,c,valores(7,1));
            z2=subs(z2,d,valores(8,1));
            z2=subs(z2,e,valores(9,1));
            z3=subs(Ep,a,valores(10,1));
            z3=subs(z3,b,valores(11,1));
            z3=subs(z3,c,valores(12,1));
            z3=subs(z3,d,valores(13,1));
            z3=subs(z3,e,valores(14,1));

            fprintf('las ecuaciones quedan: \n');

            fprintf('  en el intervalo [%f,%f] \n',itv1);
            disp(vpa(z1));

            fprintf('  en el intervalo [%f,%f] \n',itv2);
            disp(vpa(z2));

            fprintf('  en el intervalo [%f,%f] \n',itv3);
            disp(vpa(z3));


        end

        %esto de aqui grafica la funcion de manera simple

        ezplot(z1,[aa,bb]);
        ylabel('Y');
        xlabel('X');
        grid on 
        hold on 
        ezplot(z2,[aa,bb]);
        ezplot(z3,[aa,bb]);
        ezplot(f,[aa,bb]);

        
end


fprintf('\nIntegrantes:\nVladimir Enrique Martinez Flores MF18030\nMelvin Ernesto Portillo Merlos PM18008\n');