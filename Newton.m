%Newton

%esto convierte a x en una variable simbolica
syms x

f=log(x);

%aqui se pide el intervalo [a,b]
a=input('ingrese el intervalo inferior: ');

while a<=0
    fprintf('No debe ser menor o igual a cero');
    a=input('ingrese el intervalo inferior: ');
end

b=input('ingrese el intervalo superior: ');

while b<=0 || b<a
    fprintf('No debe ser menor o igual a cero o menor al anterior\n ');
    b=input('ingrese el intervalo superior: ');
end

grado=input('ingrese el grado del polinomio: ');

while grado < 1
    fprintf('Debe ser al menos 1\n');
    grado=input('ingrese el grado del polinomio: '); 
end

puntos(1)=a;
puntos(2)=b;

if(grado==1)
valoresX=puntos;

else
    while (grado+1)>numel(puntos)
        tamanio=numel(puntos);
        for i=1:1:tamanio-1
            puntos(tamanio+i)=(puntos(i)+puntos(i+1))/2;
        end

        if (grado+1)>numel(puntos)
           puntos=sort(puntos);
        else
            for i=1:1:grado+1
                valoresX(i)=puntos(i);
            end
            valoresX=sort(valoresX);
        end

    end   
end

%valores de y
for i=1:1:numel(valoresX)
   tabla(i,1)=vpa(subs(f,valoresX(i))); 
end

%Escalera
for i=2:1:grado+1
    for j=i:1:grado+1
		tabla(j,i)=(tabla(j,i-1)-tabla(j-1,i-1))/(valoresX(j)-valoresX(j-(i-1)));
    end
    
end


Px=tabla(1,1);
for i=2:1:grado+1
    dato=tabla(i,i);
    for j=1:1:grado
        dato=dato*(x-valoresX(j));    
    end
    Px=Px+dato;
end

fprintf('P(x)= ');
disp(vpa(expand(Px),15));


opcion=input('Desea calcular los errores ?\n1.Si\nOtro No\n');
if opcion==1
    clc
    valor=input('ingrese el valor de x\n');
    while(valor<a || valor>b)
       valor=input('ingrese el valor de x\n');     
    end
    P=subs(Px,valor);
    
    %Errores
    fprintf('P(%f)= ',valor);
    disp(vpa(P,15));
    vr=log(valor);
    Ev=abs(vr-P);
    Er=abs(Ev/vr);
    Ep=Er*100;
    
    derivada=f;
    
    
    for i=1:1:grado+1
        derivada=diff(derivada);
    end
    
    Et=vpa(subs(derivada,valoresX(grado+1))/(factorial(grado+2)));
    
    for i=1:1:grado+1
        Et=abs(Et*(valor-valoresX(i)));
    end
    
    fprintf('Ev= ');
    disp(vpa(Ev,15));
    
    fprintf('Er= ');
    disp(vpa(Er,15));
    
    fprintf('Ep= ');
    disp(vpa(Ep,15));
    
    fprintf('E.Teorico= ');
    disp(vpa(Et,15));
    
end

ezplot(Px,[a,b]);
ylabel('Y');
xlabel('X');
grid on 
hold on 
ezplot(f,[a,b]);



