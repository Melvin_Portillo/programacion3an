%Int. cuadratica
%esto convierte a x en una variable simbolica
syms x

f=log(x);

%aqui se pide el intervalo [a,b]
aa=input('ingrese el intervalo inferior: ');

while aa<=0
    fprintf('No debe ser menor o igual a cero');
    aa=input('ingrese el intervalo inferior: ');
end

bb=input('ingrese el intervalo superior: ');

while bb<=0 || bb<aa
    fprintf('No debe ser menor o igual a cero o menor al anterior ');
    bb=input('ingrese el intervalo superior: ');
end

valoresX(1)=aa;
valoresX(2)=(aa+bb)/2;
valoresX(3)=bb;

valoresY(1)=vpa(subs(f,valoresX(1)));
valoresY(2)=vpa(subs(f,valoresX(2)));
valoresY(3)=vpa(subs(f,valoresX(3)));

b0=valoresY(1);
b1=(valoresY(2)-valoresY(1))/(valoresX(2)-valoresX(1));
b2=((valoresY(3)-valoresY(2))/(valoresX(3)-valoresX(2))-b1)/(valoresX(3)-valoresX(1));

Px=b0+b1*(x-valoresX(1))+b2*(x-valoresX(1))*(x-valoresX(2));

fprintf('P(x)= ');
disp(vpa(expand(Px),15));
